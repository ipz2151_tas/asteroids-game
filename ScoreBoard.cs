﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASTEROIDS
{
    public class ScoreBoard
    {
        public int SCORE = 0;
        public int HIGHSCORE = 1000000;
        private int shipsLeft = 0;
        private List<Ship> ShipsLeft = new List<Ship>();
        public int SHIPSLEFT
        {
            get { return shipsLeft;  }
            set {
                shipsLeft = value;
                if(ShipsLeft.Count != shipsLeft )
                {
                    ShipsLeft.Clear();
                    for (int x = 0; x < shipsLeft; x++)
                    {
                        Ship current = new Ship(canvas);
                        current.position = new Point(445 + (x * 23), 168);
                        ShipsLeft.Add( current );
                    }
                }
                }
        }

        public int CURRENTLEVEL = 0;

        public string COPYRIGHTMESSAGE = "©2022 ANDRIY TIMCHENKO";
        public string STARTMESSAGE = "НАЖМІТЬ СТАРТ";
        public string GAMEOVERMESSAGE = "ГРА ЗАКІНЧЕНА";
        public string PLAYERMESSAGE = "ГРАВЕЦЬ 1";

        public long blinkTicks;
        public bool isPlaying = false;
        private bool blinkOn = true;

        
        private const double DELAYBETWEENHIGHSCOREDISPLAY = 16.0f;  
        private bool isHighScoreScreenVisible = true;
        public void displayHighScoreScreen()
        {
            isHighScoreScreenVisible = true;
            nextToggle = System.DateTime.Now.AddTicks((long)(DELAYBETWEENHIGHSCOREDISPLAY * (double)frmAsteroids.TICKSPERSECOND)).Ticks;
        }
        private long nextToggle;

        private frmAsteroids canvas;

        private HighScores highScores = new HighScores();
        public HighScores HighScores
        {
            get { return highScores;  } 
        }

        public ScoreBoard( frmAsteroids frm)
        {
            canvas = frm;
            nextToggle = System.DateTime.Now.AddTicks( (long) ( DELAYBETWEENHIGHSCOREDISPLAY * (double) frmAsteroids.TICKSPERSECOND ) ).Ticks;
            blinkTicks = System.DateTime.Now.AddTicks(6000000).Ticks;
        }

        private Point retainStartMsgLeft = new Point();

        public void Draw()
        {
            // Астероїди
            AsteroidsFontHandler afh = new AsteroidsFontHandler(canvas);
            System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);

            // Табло
            afh.FontSize = 32.0f; afh.Kerning = 7;
            afh.Text = SCORE.ToString("D2");
            afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2) - 410 - (int)afh.TextWidth, 60);
            afh.Draw();

            // Рейтингове табло
            afh.FontSize = 18.0f;
            afh.Kerning = 3;
            afh.Text = highScores.GetHighScore().ToString();
            afh.TextPosition = new Point((int)((canvas.Width - afh.TextWidth) / 2), 70);
            afh.Draw();

            afh.FontSize = 32.0f; afh.Kerning = 10;
            afh.Text = SCORE.ToString("D2");

            

            if (System.DateTime.Now.Ticks > blinkTicks)
            {
                blinkTicks = System.DateTime.Now.AddTicks(4750000).Ticks;
                blinkOn = !blinkOn;
            }

            if (blinkOn && !isPlaying)
            {
                
                afh.Kerning = 10;
                afh.Text = STARTMESSAGE; 
                retainStartMsgLeft = new Point(((canvas.Width - afh.TextWidth) / 2), 175);
                afh.TextPosition = retainStartMsgLeft;

                afh.Draw();
            }
            if( !isPlaying)
            {
                if (nextToggle <= System.DateTime.Now.Ticks)
                {
                    isHighScoreScreenVisible = !isHighScoreScreenVisible;

                    nextToggle = System.DateTime.Now.AddTicks((long)(DELAYBETWEENHIGHSCOREDISPLAY * (double)frmAsteroids.TICKSPERSECOND)).Ticks;
                }

                if (isHighScoreScreenVisible)
                {
                    afh.Kerning = 10;
                    afh.Text = "Високі бали"; 
                    afh.TextPosition = new Point( retainStartMsgLeft.X, retainStartMsgLeft.Y + 90 );
                    afh.Draw();

                    int x = 0;
                    foreach( HighScore hs in highScores.list.OrderByDescending( hs => hs.Score ) )
                    {
                        String replaceUnderscores = hs.Initials.Replace('_', ' ');
                        afh.Text = String.Format("{0}{1}.{2,5} {3}",
                                            ( ( x + 1 ).ToString().Length == 1 )?" ":"",
                                            x + 1,
                                            hs.Score,
                                            replaceUnderscores);
                        afh.TextPosition = new Point(retainStartMsgLeft.X-25, retainStartMsgLeft.Y + 175 + ( x * 40) );
                        afh.Draw();
                        x += 1;
                    }
                }
            }
            else if( isPlaying )
            {
                
                afh.Text = PLAYERMESSAGE; 
                afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2), 225);
                afh.Draw();

                foreach (Ship s in ShipsLeft)
                    s.Draw();
            }

            // Надпис кінцю гри
            if (canvas.gameOver && canvas.EnterHighScoreScreen.display != true )
            {
                afh.Text = GAMEOVERMESSAGE; 
                afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2), (canvas.Height / 2) - 100);
                afh.Draw();
            }


            // Надпис копірайту
            afh.FontSize = 18.0f;
            afh.Kerning = 3;
            afh.Text = COPYRIGHTMESSAGE; 
            afh.TextPosition = new Point( (int)((canvas.Width - afh.TextWidth) / 2), canvas.Height - 50);
            afh.Draw();

            
        }

    }
}
