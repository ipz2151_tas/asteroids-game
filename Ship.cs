﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASTEROIDS 
{
    class Ship
    {
        private frmAsteroids canvas;
        public float myAngle = 0.0f;
        public float accelerationAngle = 0.0f;
        public float rotationAngle;
        public double velocity = 0.0f;

        //public float velocity;
        public const double ACCELERATIONSPEED = 0.015f;
        public const double DECELERATIONSPEED = 0.0002f;
        public const double MAXVELOCITY = 0.8f;
        public const float ROTSPEED = ((float)Math.PI / 90.0f) * 2.8f;
        public const int RADIUS = 0;

        private bool  accelerating;

        private bool isRotating;
        private Keys keyDepressed;

        public Point position = new Point();
        public bool isActive = false;
        public bool isHyperSpace = false;

        private long destructionAnimationTickStart;
        private bool playerIsDestroyed = false;
        public bool bPlayerIsDestroyed
        {
            get { return playerIsDestroyed;  }
            set {
                if (value)
                    destructionAnimationTickStart = DateTime.Now.Ticks;

                playerIsDestroyed = value;
                }
        }

        public Ship( frmAsteroids frm )
        {
            canvas = frm;
            resetToCenter();
        }

        public void resetToCenter()
        {
            position.X = (canvas.Width - 15) / 2;
            position.Y = (canvas.Height + 25) / 2;
        }

        public void Draw()
        {
            if (isHyperSpace)
                return; 

            if (isRotating)
            {
                if (keyDepressed == Keys.Left)
                    accelerationAngle -= ((accelerationAngle>0.0f)  ? ROTSPEED : -((float)(Math.PI*2.0f)-ROTSPEED ));
                
                else
                    accelerationAngle += ((accelerationAngle < (Math.PI*2.0f)) ? ROTSPEED : -(accelerationAngle-ROTSPEED));

                rotationAngle = accelerationAngle;
            }

            Pen pen = new Pen(Color.White);
            if (playerIsDestroyed)
            {
                int offset = (int)((DateTime.Now.Ticks - destructionAnimationTickStart) / 500000);

                
                if (offset < 26)
                    canvas.g.DrawLine(pen,
                                        new Point(position.X - 7,
                                                  position.Y - 9 - offset),
                                        new Point(position.X + 9,
                                                  position.Y - 6 - offset));

                if (offset < 32)
                    canvas.g.DrawLine(pen,
                                    new Point(position.X - 2 + (int)(Math.Sin(Math.PI / 3) * offset),
                                              position.Y - 9 - (int)(Math.Cos(Math.PI / 3) * offset)),
                                    new Point(position.X + 15 + (int)(Math.Sin(Math.PI / 3) * offset),
                                              position.Y - (int)(Math.Cos(Math.PI / 3) * offset)));

                float sa = (float)Math.Sin(Math.PI/2);
                float ca = (float)Math.Cos(Math.PI/2);

                if (offset < 38)
                    canvas.g.DrawLine(pen,
                                    new Point(position.X + 7 + (int)(sa * offset),
                                              position.Y + (int)(ca * offset)),
                                    new Point(position.X + 15 + (int)(sa * offset),
                                              position.Y + 10 + (int)(ca * offset)));

                sa = (float)Math.Sin(Math.PI / 6);
                ca = (float)Math.Cos(Math.PI / 6);

                if (offset < 30)
                    canvas.g.DrawLine(pen,
                                    new Point(position.X + (int)(sa * offset),
                                              position.Y + 7 + (int)(ca * offset)),
                                    new Point(position.X + 10 + (int)(sa * offset),
                                              position.Y + 4 + (int)(ca * offset)));

                sa = (float)Math.Sin(Math.PI / 6);
                ca = (float)Math.Cos(Math.PI / 6);

                if (offset < 12)
                    canvas.g.DrawLine(pen,
                                    new Point(position.X - 7 - (int)(sa * offset),
                                              position.Y + 2 + (int)(ca * offset)),
                                    new Point(position.X - (int)(sa * offset),
                                              position.Y + 8 + (int)(ca * offset)));

                sa = (float)Math.Sin(Math.PI / 6);
                ca = (float)Math.Cos(Math.PI / 6);

                if (offset < 50)
                    canvas.g.DrawLine(pen,
                                    new Point(position.X - 3 - (int)(sa * offset),
                                              position.Y - 3 - (int)(ca * offset)),
                                    new Point(position.X - 10 - (int)(sa * offset),
                                              position.Y + 9 - (int)(ca * offset)));

            }
            else
            {
                float sa = (float)Math.Sin(accelerationAngle);
                float ca = (float)Math.Cos(accelerationAngle);

                //Намалювання коробля
                canvas.g.DrawLine(pen,
                                    new Point(position.X + (int)(sa * 13),
                                              position.Y - (int)(ca * 13)),
                                    new Point(position.X + (int)(ca * 10) - (int)(sa * 17),
                                              position.Y + (int)(ca * 17) + (int)(sa * 10)));
                canvas.g.DrawLine(pen,
                                    new Point(position.X + (int)(ca * 10) - (int)(sa * 17),
                                              position.Y + (int)(ca * 17) + (int)(sa * 10)),
                                    new Point(position.X + (int)(ca * 4) - (int)(sa * 12),
                                              position.Y + (int)(ca * 12) + (int)(sa * 4)));
                canvas.g.DrawLine(pen,
                                    new Point(position.X + (int)(ca * 4) - (int)(sa * 12),
                                              position.Y + (int)(ca * 12) + (int)(sa * 4)),
                                    new Point(position.X - (int)(ca * 4) - (int)(sa * 12),
                                              position.Y + (int)(ca * 12) - (int)(sa * 4)));
                canvas.g.DrawLine(pen,
                                    new Point(position.X - (int)(ca * 10) - (int)(sa * 17),
                                              position.Y + (int)(ca * 17) - (int)(sa * 10)),
                                    new Point(position.X - (int)(ca * 4) - (int)(sa * 12),
                                              position.Y + (int)(ca * 12) - (int)(sa * 4)));
                canvas.g.DrawLine(pen,
                                    new Point(position.X + (int)(sa * 13),
                                              position.Y - (int)(ca * 13)),
                                    new Point(position.X - (int)(ca * 10) - (int)(sa * 17),
                                              position.Y + (int)(ca * 17) - (int)(sa * 10)));

                
                if ( accelerating)
                {
                    canvas.g.DrawLine(pen,
                                        new Point(position.X - (int)(ca * 4) - (int)(sa * 14),
                                                  position.Y + (int)(ca * 14) - (int)(sa * 4)),
                                        new Point(position.X - (int)(sa * 23),
                                                  position.Y + (int)(ca * 23)));
                    canvas.g.DrawLine(pen,
                                        new Point(position.X + (int)(ca * 4) - (int)(sa * 14),
                                                  position.Y + (int)(ca * 14) + (int)(sa * 4)),
                                        new Point(position.X - (int)(sa * 23),
                                                  position.Y + (int)(ca * 23)));
                }

                doPhysics();
            }
        }

        public void Rotate(Keys key, bool bIsRotating )
        {
            isRotating = bIsRotating;
            keyDepressed = key;
        }

        public void Accelerate(Keys key, bool bAccelerate)
        {
             accelerating = bAccelerate;
            
        }

        private void doPhysics()
        {
            
            if (! accelerating)
            {
                if (velocity > 0.0f)
                    velocity -= DECELERATIONSPEED;
                else
                    velocity = 0.0f;
            }
            
            
            else if (velocity == 0.0f) 
            {
                velocity += ACCELERATIONSPEED;
                myAngle = accelerationAngle;
            }
            
            else if ( ((velocity + ACCELERATIONSPEED) < MAXVELOCITY)
                      && ( myAngle == accelerationAngle ) )
            {
                velocity += ACCELERATIONSPEED;
            }
            else 
            {
                // Обчислює існуючі очікувані зміщення від поточної позиції
                double vfCX = velocity * Math.Cos(myAngle);
                double vfCY = velocity * Math.Sin(myAngle);
                // Обчислює зміну положення на основі напрямку тяги
                double vfNX = ACCELERATIONSPEED * Math.Cos(accelerationAngle);
                double vfNY = ACCELERATIONSPEED * Math.Sin(accelerationAngle);
                
                if ((Math.Sqrt(Math.Pow(vfCX + vfNX, 2) + Math.Pow(vfCY + vfNY, 2)) < MAXVELOCITY))
                    velocity = Math.Sqrt(Math.Pow(vfCX + vfNX, 2) + Math.Pow(vfCY + vfNY, 2));

                
                myAngle =  (float)Math.Atan2((vfCY + vfNY),(vfCX + vfNX));
                
            }

            float sa = (float)Math.Sin(myAngle);
            float ca = (float)Math.Cos(myAngle);

            position = new Point(position.X + (int)(sa * velocity * 15),
                                 position.Y - (int)(ca * velocity * 15));
            if (position.X < 0)
                position = new Point(canvas.Width + position.X, position.Y);
            else if (position.X > canvas.Width)
                position = new Point(position.X - canvas.Width, position.Y);
            if (position.Y < 0)
                position = new Point(position.X, canvas.Height + position.Y);
            else if (position.Y > canvas.Height)
                position = new Point(position.X, position.Y - canvas.Height);

            
        }
        public bool doesObjectCollide(Point p, float objectradius )
        {
            float checkValue = (float)Math.Sqrt(Math.Pow(position.X - p.X, 2) + Math.Pow(position.Y - p.Y, 2));
            if (checkValue < (objectradius + RADIUS + 10))
                return true;
            else if (objectradius < 10)
                throw new Exception("Не повинно бути!");

            return false;
        }
    }
}
